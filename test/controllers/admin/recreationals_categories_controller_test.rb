require 'test_helper'

class Admin::RecreationalsCategoriesControllerTest < ActionController::TestCase
  setup do
    @admin_recreationals_category = admin_recreationals_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_recreationals_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_recreationals_category" do
    assert_difference('Admin::RecreationalsCategory.count') do
      post :create, admin_recreationals_category: { description: @admin_recreationals_category.description, image: @admin_recreationals_category.image, name: @admin_recreationals_category.name, status: @admin_recreationals_category.status }
    end

    assert_redirected_to admin_recreationals_category_path(assigns(:admin_recreationals_category))
  end

  test "should show admin_recreationals_category" do
    get :show, id: @admin_recreationals_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_recreationals_category
    assert_response :success
  end

  test "should update admin_recreationals_category" do
    patch :update, id: @admin_recreationals_category, admin_recreationals_category: { description: @admin_recreationals_category.description, image: @admin_recreationals_category.image, name: @admin_recreationals_category.name, status: @admin_recreationals_category.status }
    assert_redirected_to admin_recreationals_category_path(assigns(:admin_recreationals_category))
  end

  test "should destroy admin_recreationals_category" do
    assert_difference('Admin::RecreationalsCategory.count', -1) do
      delete :destroy, id: @admin_recreationals_category
    end

    assert_redirected_to admin_recreationals_categories_path
  end
end
