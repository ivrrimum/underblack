require 'test_helper'

class Admin::InventoryItemsControllerTest < ActionController::TestCase
  setup do
    @admin_inventory_item = admin_inventory_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_inventory_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_inventory_item" do
    assert_difference('Admin::InventoryItem.count') do
      post :create, admin_inventory_item: { image: @admin_inventory_item.image, name: @admin_inventory_item.name, status: @admin_inventory_item.status }
    end

    assert_redirected_to admin_inventory_item_path(assigns(:admin_inventory_item))
  end

  test "should show admin_inventory_item" do
    get :show, id: @admin_inventory_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_inventory_item
    assert_response :success
  end

  test "should update admin_inventory_item" do
    patch :update, id: @admin_inventory_item, admin_inventory_item: { image: @admin_inventory_item.image, name: @admin_inventory_item.name, status: @admin_inventory_item.status }
    assert_redirected_to admin_inventory_item_path(assigns(:admin_inventory_item))
  end

  test "should destroy admin_inventory_item" do
    assert_difference('Admin::InventoryItem.count', -1) do
      delete :destroy, id: @admin_inventory_item
    end

    assert_redirected_to admin_inventory_items_path
  end
end
