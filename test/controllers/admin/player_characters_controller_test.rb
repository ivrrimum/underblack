require 'test_helper'

class Admin::PlayerCharactersControllerTest < ActionController::TestCase
  setup do
    @admin_player_character = admin_player_characters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_player_characters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_player_character" do
    assert_difference('Admin::PlayerCharacter.count') do
      post :create, admin_player_character: { description: @admin_player_character.description, image: @admin_player_character.image, name: @admin_player_character.name }
    end

    assert_redirected_to admin_player_character_path(assigns(:admin_player_character))
  end

  test "should show admin_player_character" do
    get :show, id: @admin_player_character
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_player_character
    assert_response :success
  end

  test "should update admin_player_character" do
    patch :update, id: @admin_player_character, admin_player_character: { description: @admin_player_character.description, image: @admin_player_character.image, name: @admin_player_character.name }
    assert_redirected_to admin_player_character_path(assigns(:admin_player_character))
  end

  test "should destroy admin_player_character" do
    assert_difference('Admin::PlayerCharacter.count', -1) do
      delete :destroy, id: @admin_player_character
    end

    assert_redirected_to admin_player_characters_path
  end
end
