require 'test_helper'

class Admin::RecreationalResourcesControllerTest < ActionController::TestCase
  setup do
    @admin_recreational_resource = admin_recreational_resources(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_recreational_resources)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_recreational_resource" do
    assert_difference('Admin::RecreationalResource.count') do
      post :create, admin_recreational_resource: { cat_id: @admin_recreational_resource.cat_id, description: @admin_recreational_resource.description, name: @admin_recreational_resource.name, per_hour: @admin_recreational_resource.per_hour, price_money: @admin_recreational_resource.price_money, price_non_free: @admin_recreational_resource.price_non_free }
    end

    assert_redirected_to admin_recreational_resource_path(assigns(:admin_recreational_resource))
  end

  test "should show admin_recreational_resource" do
    get :show, id: @admin_recreational_resource
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_recreational_resource
    assert_response :success
  end

  test "should update admin_recreational_resource" do
    patch :update, id: @admin_recreational_resource, admin_recreational_resource: { cat_id: @admin_recreational_resource.cat_id, description: @admin_recreational_resource.description, name: @admin_recreational_resource.name, per_hour: @admin_recreational_resource.per_hour, price_money: @admin_recreational_resource.price_money, price_non_free: @admin_recreational_resource.price_non_free }
    assert_redirected_to admin_recreational_resource_path(assigns(:admin_recreational_resource))
  end

  test "should destroy admin_recreational_resource" do
    assert_difference('Admin::RecreationalResource.count', -1) do
      delete :destroy, id: @admin_recreational_resource
    end

    assert_redirected_to admin_recreational_resources_path
  end
end
