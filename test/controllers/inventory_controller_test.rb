require 'test_helper'

class InventoryControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get collect_loot" do
    get :collect_loot
    assert_response :success
  end

end
