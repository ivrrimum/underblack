// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jqueryui
//= require turbolinks
//= require bootstrap


$(document).on('page:change', function() {
	$( ".dragable" ).draggable({ revert: "invalid" });

	$( ".drop" ).droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      drop: function( event, ui ) {
        // Checks if he wants to change gear
        if ($(this).attr("class") == "drop gear_item ui-droppable") {
          console.log("asddsas");
        };

        // Sets variables for ajax
        box = $(this).attr("box_id");
        item_id = $(ui.draggable).attr("item_id");

        $(ui.draggable).offset($(this).offset());
        // Sends Ajax to update inventory place
        $.ajax({
		    type: "POST",
		    url: "/inventory/updateInventory",
		    data: { box: box, item_id: item_id },
		    success: function(data){
		        console.log("Droped!");
		    }        
		});
      }
    });
    });