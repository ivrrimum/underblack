class Admin::PlayerCharacter < ActiveRecord::Base

	def self.GetPlayerCharacterName id
		# Fetches Character
		get_name = Admin::PlayerCharacter.find_by_id(id)
		# return name
		get_name.name
	end

end
