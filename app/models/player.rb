class Player < ActiveRecord::Base
  # Serialization
  serialize :money
  serialize :recreational_resources
  serialize :restime
  serialize :gear
  serialize :inventory
  # Define Validation
  validates :username, length: { minimum: 3 }, uniqueness: true
  validates :email, length: { minimum: 4 }, uniqueness: true
  validates :password, length: { minimum: 5 }

  # Encripts password and generates session id( and logs him in )
  before_create :encrypt_password, :generate_session_id

	# Checks if there is a user in database with username and password like that
	def self.auth username, password
		# Fetches data
		validateUser = Player.where( :username => username, :password => Digest::SHA1.hexdigest(password) )
		# Counts results and return true or false
  		if validateUser.count == 1
  			# gets user id from current object
  			id = 0;
  			validateUser.each do |dd|
  				id = dd.id
  			end
  			# Because of successfull login updates session_id
  			editUser = Player.find_by_id(id)
  				editUser.session_id = Digest::SHA1.hexdigest("#{Time.now.to_i}#{password}")
  			editUser.save()
  		  	# returns session_id - authorification is successfull		
  			Digest::SHA1.hexdigest("#{Time.now.to_i}#{password}")	
  		else
  			false
  		end
	end

  # Encripts password
  def encrypt_password
      self.password = Digest::SHA1.hexdigest(self.password)
  end

  # Generates Session id
  def generate_session_id
      self.session_id = Digest::SHA1.hexdigest("#{Time.now.to_i}#{self.password}")
  end

  # Return item object from inventory
  def self.returnItemObjectsFromInventory id, user
    ret = 0
    # Loops trought inventory
    user.inventory.each do |key, value|
      value.each do |item_id, item_spec|
        if item_id == id
          ret = value
        end
      end
    end
    ret
  end

end
