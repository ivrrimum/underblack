json.array!(@admin_player_characters) do |admin_player_character|
  json.extract! admin_player_character, :id, :name, :description, :image
  json.url admin_player_character_url(admin_player_character, format: :json)
end
