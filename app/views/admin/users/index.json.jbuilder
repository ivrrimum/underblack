json.array!(@players) do |player|
  json.extract! player, :id, :username, :password, :email, :session_id
  json.url player_url(player, format: :json)
end
