json.array!(@admin_recreational_resources) do |admin_recreational_resource|
  json.extract! admin_recreational_resource, :id, :cat_id, :name, :description, :per_hour, :price_money, :price_non_free
  json.url admin_recreational_resource_url(admin_recreational_resource, format: :json)
end
