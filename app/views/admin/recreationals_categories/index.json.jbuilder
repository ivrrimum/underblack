json.array!(@admin_recreationals_categories) do |admin_recreationals_category|
  json.extract! admin_recreationals_category, :id, :name, :description, :image, :status
  json.url admin_recreationals_category_url(admin_recreationals_category, format: :json)
end
