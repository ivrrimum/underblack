class ResourcesController < ApplicationController
  before_filter :logged

  def index
  	@cat = Admin::RecreationalsCategory.where( :status => 1, :character => @CurrentUser.character )
  end

  def viewResource
  	# Gets ID paramterer
  	@id = params[:id].to_i
  	# Checks if it is set
  	if @id
  		# Fetches Categorie fields and resour
  		@cat = Admin::RecreationalsCategory.find_by_id(@id)
  		# If it is found, then fetches resources, if not resirects with error
  		if @cat.nil?
  			redirect_to "/", :notice => "Given Resource Categorie doesnt exist"
  		else
  			@resources = Admin::RecreationalResource.where( :cat_id => @id )
  		end
  	end
  end

  def buyResource
    # Fetch resource ID and quantity
      id = params["resource_id"].to_i
      quantity = params['quantity'].to_i
    # Checks if they are set
    if id && quantity
      # Checks type of payment
      if params['money']
        money = @CurrentUser.money[:money]
      else
        money = @CurrentUser.money[:non_free]
      end
      # Fetches Resource with coresponding link
      resource = Admin::RecreationalResource.find_by_id(id)
      # Checks if found
      if !resource.nil?
        # Gets price for coresponding type
        if params['money']
          price = resource.price_money * quantity.to_i
        else
          price = resource.price_non_free * quantity.to_i
        end
        # Checks if user has enought money( with selected valute ) to buy item with quantity
        if money < price
          redirect_to "/resources/viewResource/#{resource.cat_id}", :notice => "You dont have enought balance to buy this item."
        else
          # Updates valute money from users pocket
          if params['money']
            @CurrentUser.money[:money] = money - price
          else
            @CurrentUser.money[:non_free] = money - price
          end
          # Checks if user alredy have products from that cat
          if @CurrentUser.recreational_resources[resource.cat_id].nil?
            @CurrentUser.recreational_resources[resource.cat_id] = {}
          end
          # Updates user profile, adds product to user's profile
          @CurrentUser.recreational_resources[resource.cat_id][resource.id] = quantity.to_i + @CurrentUser.recreational_resources[resource.cat_id][resource.id].to_i
          # Checks if user alredy have timer from that cat
          if @CurrentUser.restime[resource.cat_id].nil?
            @CurrentUser.restime[resource.cat_id] = {}
          end
          # Revenue timer start
          @CurrentUser.restime[resource.cat_id][resource.id] = Time.now.to_i 
            @CurrentUser.save
          # Redirect With a message for Successfull bought'h
          redirect_to "/resources/viewResource/#{resource.cat_id}", :notice => "You have successfully bought #{quantity.to_i} #{resource.name}."
        end
      else
        redirect_to "/resources", :notice => "Resouce doesnt exist."
      end
    end
  end

  def collectMoney
    # Fetches resource categorie id
    id = params[:id].to_i
    # Checks if it is set
    if id
      # Checks if user has any items in this cat
      if @CurrentUser.restime[id].nil?
        redirect_to "/resources/viewResource/#{id}", :notice => "You have no money to collect."
      else
        # Sets variable where total income is going to be calculated
        total_income = 0
        # Calculates total income and resets counters of items
        @CurrentUser.restime[id].each do |key, value|
          # Fetches resource
          res = Admin::RecreationalResource.find_by_id(key.to_i)
          # Calculate income
          summ = ((Time.now.to_i - @CurrentUser.restime[id][key].to_i) / 3600) * res.per_hour.to_i
          # Adds to summ
          total_income += summ
          # Resets timer
          @CurrentUser.restime[id][key] = Time.now.to_i
        end
        # Adds money to user
        @CurrentUser.money[:money] = total_income + @CurrentUser.money[:money].to_i
        @CurrentUser.save 
        redirect_to "/resources/viewResource/#{id}", :notice => "You have successfully collected your money, your income: #{total_income}."
      end
    end
  end

end
