class InventoryController < ApplicationController
  before_filter :logged

  def index
  	# Fetch Gear for current user
  	@gear = @CurrentUser.gear
  	# Makes Player object
  	@player = Player
  	# Gets count of inventory tiles
  	@inventory_blocks = Underblack::Application.config.inventory_blocks

  	#@CurrentUser.inventory = {}
  	#@CurrentUser.inventory[:page1] = { 100 => { :item_id => 2, :level => 12, :location => 3 }, 
  	#									8 => { :item_id => 3, :level => 13, :location => 6 } }
  	#@CurrentUser.save
  end

  def updateInventory
  	# Fetches variables for update
  	box = params[:box].to_i
  	item_id = params[:item_id].to_i
  	# Checks if they are set
  	if box && item_id
  		# Sets exist to false, if item doesnt exist
  		exists = false
		# Returns true if item is found
		@CurrentUser.inventory.map do |pages, item_object|
			item_object.map do |id_of_item, properties|
				# Checks if item id, is same as requested item id
				if item_id == id_of_item
					exists = true
				end
			end
		end
		# Checks if item was found
		if exists
			# Check if box param is valid
			if box <= 12 && box >= 1
				# sets dummie page
				page = 0
				# Finds page with item is located in
				@CurrentUser.inventory.map do |pages, item_object|
					item_object.map do |id_of_item, properties|
						# Checks if item id, is same as requested item id
						if item_id == id_of_item
							page = pages
						end
					end
				end
			end
			# Changes location of item
			@CurrentUser.inventory[page][item_id][:location] = box
			# Saves changes
			@CurrentUser.save
		end
  	end

  end

  def collect_loot
  end
end
