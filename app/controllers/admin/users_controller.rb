class Admin::UsersController < ApplicationController
  before_filter :is_admin
  before_action :set_player, only: [:show, :edit, :update, :destroy]

  # GET /players
  # GET /players.json
  def index
    @players = Player.all
  end

  # GET /players/1
  # GET /players/1.json
  def show
  end

  # GET /players/new
  def new
    edit = Player.new

    username = params[:username]
    password = params[:password]
    email = params[:email]
    if !username.nil? && !email.nil?
    edit.username = username
    if password != ""
      edit.password = Digest::SHA1.hexdigest(password)
    end
    edit.email = email
    edit.save
    redirect_to "/admin/users", :notice => "User created successfully"       
    end

  end

  # GET /players/1/edit
  def edit
    @id = params[:id]
    @players = Player.find_by_id(@id)
    @player = Player.where( :id => @id )
    username = params[:username]
    password = params[:password]
    email = params[:email]
    if !username.nil? && !email.nil?
    edit = Player.where( :id => @id ).first
    edit.username = username
    if password != ""
      edit.password = Digest::SHA1.hexdigest(password)
    end
    edit.email = email
    edit.save
    redirect_to "/admin/users", :notice => "User edited successfully"       
    end



  end

  # POST /players
  # POST /players.json
  def create
    @player = Player.new(player_params)

    respond_to do |format|
      if @player.save
        format.html { redirect_to @player, notice: 'Player was successfully created.' }
        format.json { render :show, status: :created, location: @player }
      else
        format.html { render :new }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /players/1
  # PATCH/PUT /players/1.json
  def update

  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    @player.destroy
    respond_to do |format|
      format.html { redirect_to "/admin/users", notice: 'Player was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player
      @player = Player.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def player_params
      params.require(:player).permit(:username, :password, :email, :session_id)
    end
end
