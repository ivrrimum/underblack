class Admin::RecreationalResourcesController < ApplicationController
  before_filter :is_admin
  before_action :set_admin_recreational_resource, only: [:show, :edit, :update, :destroy]

  # GET /admin/recreational_resources
  # GET /admin/recreational_resources.json
  def index
    @admin_recreational_resources = Admin::RecreationalResource.all
    @GetCatname = Admin::RecreationalsCategory
    @GetCharname = Admin::PlayerCharacter
  end

  # GET /admin/recreational_resources/1
  # GET /admin/recreational_resources/1.json
  def show
  end

  # GET /admin/recreational_resources/new
  def new
    @admin_recreational_resource = Admin::RecreationalResource.new
  end

  # GET /admin/recreational_resources/1/edit
  def edit
  end

  # POST /admin/recreational_resources
  # POST /admin/recreational_resources.json
  def create
    @admin_recreational_resource = Admin::RecreationalResource.new(admin_recreational_resource_params)

    respond_to do |format|
      if @admin_recreational_resource.save
        format.html { redirect_to @admin_recreational_resource, notice: 'Recreational resource was successfully created.' }
        format.json { render :show, status: :created, location: @admin_recreational_resource }
      else
        format.html { render :new }
        format.json { render json: @admin_recreational_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/recreational_resources/1
  # PATCH/PUT /admin/recreational_resources/1.json
  def update
    respond_to do |format|
      if @admin_recreational_resource.update(admin_recreational_resource_params)
        format.html { redirect_to @admin_recreational_resource, notice: 'Recreational resource was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_recreational_resource }
      else
        format.html { render :edit }
        format.json { render json: @admin_recreational_resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/recreational_resources/1
  # DELETE /admin/recreational_resources/1.json
  def destroy
    @admin_recreational_resource.destroy
    respond_to do |format|
      format.html { redirect_to admin_recreational_resources_url, notice: 'Recreational resource was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_recreational_resource
      @admin_recreational_resource = Admin::RecreationalResource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_recreational_resource_params
      params.require(:admin_recreational_resource).permit(:cat_id, :name, :description, :per_hour, :price_money, :price_non_free)
    end
end
