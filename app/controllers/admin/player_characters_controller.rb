class Admin::PlayerCharactersController < ApplicationController
  before_filter :is_admin
  before_action :set_admin_player_character, only: [:show, :edit, :update, :destroy]

  # GET /admin/player_characters
  # GET /admin/player_characters.json
  def index
    @admin_player_characters = Admin::PlayerCharacter.all
  end

  # GET /admin/player_characters/1
  # GET /admin/player_characters/1.json
  def show
  end

  # GET /admin/player_characters/new
  def new
    @admin_player_character = Admin::PlayerCharacter.new
  end

  # GET /admin/player_characters/1/edit
  def edit
  end

  # POST /admin/player_characters
  # POST /admin/player_characters.json
  def create
    @admin_player_character = Admin::PlayerCharacter.new(admin_player_character_params)

    respond_to do |format|
      if @admin_player_character.save
        format.html { redirect_to @admin_player_character, notice: 'Player character was successfully created.' }
        format.json { render :show, status: :created, location: @admin_player_character }
      else
        format.html { render :new }
        format.json { render json: @admin_player_character.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/player_characters/1
  # PATCH/PUT /admin/player_characters/1.json
  def update
    respond_to do |format|
      if @admin_player_character.update(admin_player_character_params)
        format.html { redirect_to @admin_player_character, notice: 'Player character was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_player_character }
      else
        format.html { render :edit }
        format.json { render json: @admin_player_character.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/player_characters/1
  # DELETE /admin/player_characters/1.json
  def destroy
    @admin_player_character.destroy
    respond_to do |format|
      format.html { redirect_to admin_player_characters_url, notice: 'Player character was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_player_character
      @admin_player_character = Admin::PlayerCharacter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_player_character_params
      params.require(:admin_player_character).permit(:name, :description, :image)
    end
end
