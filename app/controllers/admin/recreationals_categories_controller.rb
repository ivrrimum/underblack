class Admin::RecreationalsCategoriesController < ApplicationController
  before_filter :is_admin
  before_action :set_admin_recreationals_category, only: [:show, :edit, :update, :destroy]

  # GET /admin/recreationals_categories
  # GET /admin/recreationals_categories.json
  def index
    @admin_recreationals_categories = Admin::RecreationalsCategory.all
  end

  # GET /admin/recreationals_categories/1
  # GET /admin/recreationals_categories/1.json
  def show
  end

  # GET /admin/recreationals_categories/new
  def new
    @admin_recreationals_category = Admin::RecreationalsCategory.new
  end

  # GET /admin/recreationals_categories/1/edit
  def edit
  end

  # POST /admin/recreationals_categories
  # POST /admin/recreationals_categories.json
  def create
    @admin_recreationals_category = Admin::RecreationalsCategory.new(admin_recreationals_category_params)

    respond_to do |format|
      if @admin_recreationals_category.save
        format.html { redirect_to @admin_recreationals_category, notice: 'Recreationals category was successfully created.' }
        format.json { render :show, status: :created, location: @admin_recreationals_category }
      else
        format.html { render :new }
        format.json { render json: @admin_recreationals_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/recreationals_categories/1
  # PATCH/PUT /admin/recreationals_categories/1.json
  def update
    respond_to do |format|
      if @admin_recreationals_category.update(admin_recreationals_category_params)
        format.html { redirect_to @admin_recreationals_category, notice: 'Recreationals category was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_recreationals_category }
      else
        format.html { render :edit }
        format.json { render json: @admin_recreationals_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/recreationals_categories/1
  # DELETE /admin/recreationals_categories/1.json
  def destroy
    @admin_recreationals_category.destroy
    respond_to do |format|
      format.html { redirect_to admin_recreationals_categories_url, notice: 'Recreationals category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_recreationals_category
      @admin_recreationals_category = Admin::RecreationalsCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_recreationals_category_params
      params.require(:admin_recreationals_category).permit(:name, :description, :image, :status)
    end
end
