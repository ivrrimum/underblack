class TutorialController < ApplicationController
  before_filter :logged, :checkIfTutorialIsAlredyCompleated

  def index
  end

  def step2
  	@get_all_characters = Admin::PlayerCharacter.all
  end

  def compleate
  	# Gets character mode
  	character_value = params[:character_value]
  	# Checks if it is set
  	if !character_value.nil?
  		# Validate if character exists
  		char = Admin::PlayerCharacter.find_by_id(character_value)
  		if !char.nil?
  			# Updates user table with disabled tutorial and new character
  			edit = Player.find_by_id(@CurrentUser.id)
  			edit.tutorial = 0
  			edit.character = character_value
  			edit.save

  			redirect_to "/", :notice => "You have successfuly compleated the tutorial.";
  		else
  			redirect_to "/tutorial"
  		end
  	end 
  end

  private

  def checkIfTutorialIsAlredyCompleated
    if @CurrentUser.tutorial == 0
      redirect_to "/", :notice => "You have alredy done tutorial."
    end
  end

end
