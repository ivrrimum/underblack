class HomeController < ApplicationController
	protect_from_forgery :except => [:login, :create_user]

  def login
    # Gets POST parameters
    username = params[:username]
    password = params[:password]
    # Checks if parameters are not nil ( if they exist )
    if !username.nil? && !password.nil?
      # Searches for user with username and password
      validate = Player.auth username, password
      # If true makes session, if false, flashes
      if validate != false
        session[:auth_id] = validate
        # Refreshes page
        redirect_to "/"
      else
        redirect_to "/", :notice => "Wrong username/password."
      end
    end
  end

  def register
    # Checks if user is alredy loged in
    if @CurrentUser
      redirect_to "/", :notice => "You are alredy registered and loged in."
    end
  end

  def create_user
    # Sets variables
    username = params[:username]
    password = params[:password]
    password_comfr = params[:password_comfr]
    email = params[:email]
    # Checks if the values are set
    if !username.nil? && !password.nil? && !password_comfr.nil? && !email.nil?
      if password_comfr != password
        redirect_to "/home/register", :notice => "Password and Password comfrimation fields must mach."
      else
        # Creates new Player object and sets its abstract values
        player = Player.new
          player.username = username
          player.password = password
          player.email = email
          player.permisions = 1
          player.tutorial = 1
          player.money = {:money => 0, :non_free => 0}
          player.recreational_resources = {}
          player.restime = {}
          player.gear = { :head_set => 0, 
                          :chest_set => 0,
                          :leg_set => 0,
                          :boots_set => 0,
                          :weapon => 0,
                          :device_1 => 0,
                          :device_2 => 0 }
          player.inventory = {}
        # Validates - if fails validation displays errors, if not adds user
        if !player.valid?
          redirect_to "/home/register", :notice => player.errors.full_messages.join("<br>").html_safe
        else
          player.save
          redirect_to "/", :notice => "You have registered successfully, please log in."
        end
      end
    end
  end

  def logout
    # Destroys user sessions - logs out
    session[:auth_id] = nil
    # Makes a flash message
    flash[:notice] = "You loged Out."
    # Redirects to Main page
    redirect_to "/"
  end

end
