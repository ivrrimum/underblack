class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :construct
  
  # Checks if user is alredy loged in
  def construct
  	# Get session
  	session_id = session[:auth_id]
  	# Find user
  	securityValidation = Player.where(:session_id => session_id).first
  	# Creates user objects
  	if securityValidation
  		@CurrentUser = securityValidation
      # Checks if user have done the tutorial stuff
      if @CurrentUser.tutorial == 1
        if params[:controller] != "tutorial"
          redirect_to "/tutorial"
        end
      end
  	else
  		@CurrentUser = false
  	end
  end

  # Checks user permisions
  def is_admin
    if @CurrentUser
      if @CurrentUser.permisions == 2
      else
        redirect_to "/", :notice => "You dont have permisions to view this page.";
      end
    else
      redirect_to "/", :notice => "You dont have permisions to view this page.";
    end
  end


  # Checks if user is loged in and not banned
  def logged
    if @CurrentUser
      if [1,2].include?(@CurrentUser.permisions)
      else
        redirect_to "/", :notice => "You dont have permisions to view this page.";
      end
    else
      redirect_to "/", :notice => "You dont have permisions to view this page.";
    end
  end

end
