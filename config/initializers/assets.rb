# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w( home.css )
Rails.application.config.assets.precompile += %w( admin.css )
Rails.application.config.assets.precompile += %w( tutorial.css )
Rails.application.config.assets.precompile += %w( resources.css )
Rails.application.config.assets.precompile += %w( inventory.css )

Rails.application.config.assets.precompile += %w( home.js )
Rails.application.config.assets.precompile += %w( admin.js )
Rails.application.config.assets.precompile += %w( tutorial.js )
Rails.application.config.assets.precompile += %w( resources.js )
Rails.application.config.assets.precompile += %w( inventory.js )

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
