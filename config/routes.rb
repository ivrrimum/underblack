Rails.application.routes.draw do
  resources :users

  # Sets root path
  root 'home#index'

  # Home controller routes
  namespace :home do
      # GET
      get :index
      get :register
      get :logout
      # POST
      post :login
      post :create_user
  end
    # ROOT path for home controller
    get '/home', :controller => "home", :action => "index" 

  # Resouces public view routes
   namespace :resources do 
      # GET
      get :index
      get :viewResource
      get :collectMoney
      # POST
      post :buyResource
  end
    # ROOT path for resource controller
    get '/resources', :controller => "resources", :action => "index" 
    # Dinamic IDs for resource controller
    get 'resources/viewResource/:id' => 'resources#viewResource'
    get 'resources/collectMoney/:id' => 'resources#collectMoney'

  # Tutorial controller routes
  namespace :tutorial do
      # GET
      get :index
      get :step2
      # POST
      post :compleate
  end
    # ROOT path for tutorial controller
    get '/tutorial', :controller => "tutorial", :action => "index" 

  # Inventory controller routes
  namespace :inventory do
      # GET
      get :index
      get :collect_loot
      # POST
      post :updateInventory
  end
    # ROOT path for tutorial controller
    get '/inventory', :controller => "inventory", :action => "index" 

  # Admin controller routes
  get "/admin", :controller => "admin", :action => "dashboard"
    namespace :admin do
      resources :users
    end

    namespace :admin do
      resources :player_characters
    end

    namespace :admin do
      resources :recreationals_categories
    end

    namespace :admin do
      resources :recreational_resources
    end

    namespace :admin do
      resources :inventory_items
    end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
