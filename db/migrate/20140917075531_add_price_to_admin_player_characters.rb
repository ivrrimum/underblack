class AddPriceToAdminPlayerCharacters < ActiveRecord::Migration
  def change
    add_column :admin_player_characters, :price, :string
  end
end
