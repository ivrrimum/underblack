class AddCharacterToAdminRecreationalResources < ActiveRecord::Migration
  def change
    add_column :admin_recreational_resources, :characters, :integer
  end
end
