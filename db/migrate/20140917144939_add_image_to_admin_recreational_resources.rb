class AddImageToAdminRecreationalResources < ActiveRecord::Migration
  def change
    add_column :admin_recreational_resources, :image, :string
  end
end
