class CreateAdminPlayerCharacters < ActiveRecord::Migration
  def change
    create_table :admin_player_characters do |t|
      t.string :name
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
