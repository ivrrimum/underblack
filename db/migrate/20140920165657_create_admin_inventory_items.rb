class CreateAdminInventoryItems < ActiveRecord::Migration
  def change
    create_table :admin_inventory_items do |t|
      t.string :name
      t.string :image
      t.integer :status

      t.timestamps
    end
  end
end
