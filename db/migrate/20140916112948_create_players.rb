class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :username
      t.string :password
      t.string :email
      t.string :session_id

      t.timestamps
    end
  end
end
