class AddInventoryToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :inventory, :text
  end
end
