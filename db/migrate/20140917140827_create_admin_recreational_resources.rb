class CreateAdminRecreationalResources < ActiveRecord::Migration
  def change
    create_table :admin_recreational_resources do |t|
      t.integer :cat_id
      t.string :name
      t.text :description
      t.integer :per_hour
      t.integer :price_money
      t.integer :price_non_free

      t.timestamps
    end
  end
end
