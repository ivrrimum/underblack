class CreateAdminRecreationalsCategories < ActiveRecord::Migration
  def change
    create_table :admin_recreationals_categories do |t|
      t.string :name
      t.text :description
      t.string :image
      t.string :status

      t.timestamps
    end
  end
end
