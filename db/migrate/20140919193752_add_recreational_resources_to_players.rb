class AddRecreationalResourcesToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :recreational_resources, :text
  end
end
