# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140920180414) do

  create_table "admin_inventory_items", force: true do |t|
    t.string   "name"
    t.string   "image"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admin_player_characters", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "price"
  end

  create_table "admin_recreational_resources", force: true do |t|
    t.integer  "cat_id"
    t.string   "name"
    t.text     "description"
    t.integer  "per_hour"
    t.integer  "price_money"
    t.integer  "price_non_free"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "characters"
    t.string   "image"
  end

  create_table "admin_recreationals_categories", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "character"
  end

  create_table "players", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "email"
    t.string   "session_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "permisions"
    t.integer  "tutorial"
    t.integer  "character"
    t.text     "recreational_resources"
    t.text     "money"
    t.text     "restime"
    t.text     "gear"
    t.text     "inventory"
  end

end
